package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.command.AbstractCommand;
import ru.shchurin.tm.entity.User;

import java.util.List;

public interface ServiceLocator {
    @NotNull
    ProjectService getProjectService();

    @NotNull
    TaskService getTaskService();

    @NotNull
    UserService getUserService();
}
