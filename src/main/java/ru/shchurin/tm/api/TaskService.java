package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.Task;

import java.util.List;

public interface TaskService extends Service<Task>{
    @NotNull
    List<Task> findAll(@Nullable String userId) throws Exception;

    @NotNull
    Task findOne(@Nullable String userId, @Nullable String id) throws Exception;

    void remove(@Nullable String userId, @Nullable String id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    void removeByName(@Nullable String userId, @Nullable String name) throws Exception;

    @NotNull
    List<Task> findByName(@Nullable final String name, @NotNull final String currentUserId)
            throws Exception;

    @NotNull
    List<Task> findByDescription(@Nullable final String description, @NotNull final String currentUserId)
            throws Exception;
}
