package ru.shchurin.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.entity.AbstractEntity;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.AlreadyExistsException;
import ru.shchurin.tm.exception.ConsoleHashPasswordException;
import ru.shchurin.tm.exception.ConsoleLoginException;

import java.util.Collection;

public interface UserRepository extends Repository<User>{
    @NotNull
    Collection<User> findAll();

    @Nullable
    User findOne(@NotNull String id);

    void remove(@NotNull String id);

    void removeAll();

    void removeByLogin(@NotNull String login);

    boolean updatePassword(@NotNull String login, @NotNull String hashPassword, @NotNull String newHashPassword);
}
