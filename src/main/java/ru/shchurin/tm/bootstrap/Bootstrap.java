package ru.shchurin.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.shchurin.tm.api.*;
import ru.shchurin.tm.command.*;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.repository.*;
import ru.shchurin.tm.service.*;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.HashUtil;

import java.util.*;

public final class Bootstrap implements ServiceLocator {
    @NotNull
    private static final String GREETING = "*** WELCOME TO TASK MANAGER ***";

    @NotNull
    private static final String EXIT = "exit";

    @NotNull
    private static final String ADMIN = "Admin";

    @NotNull
    private static final String USER = "User";

    @NotNull
    private final ProjectRepository projectRepository = new ProjectRepositoryImpl();

    @NotNull
    private final TaskRepository taskRepository = new TaskRepositoryImpl();

    @NotNull
    private final UserRepository userRepository = new UserRepositoryImpl();

    @NotNull
    private final ProjectService projectService = new ProjectServiceImpl(projectRepository, taskRepository);

    @NotNull
    private final TaskService taskService = new TaskServiceImpl(taskRepository);

    @NotNull
    private final UserService userService = new UserServiceImpl(userRepository);

    @NotNull
    private final Map<String, AbstractCommand> commands = new HashMap<>();

    @Nullable
    private User currentUser;

    @Nullable
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.shchurin.tm").getSubTypesOf(AbstractCommand.class);


    private void register(@Nullable final Class clazz) throws Exception {
        if (clazz == null) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = (AbstractCommand) clazz.newInstance();

        @Nullable final String cliCommand = command.getCommand();
        @Nullable final String cliDescription = command.getDescription();
        if (cliCommand == null || cliCommand.isEmpty())
            throw new CommandCorruptException();
        if (cliDescription == null || cliDescription.isEmpty())
            throw new CommandCorruptException();
        command.setServiceLocator(this);
        commands.put(cliCommand, command);
    }

    public void init() throws Exception {
        if (classes == null) return;
        for (@Nullable final Class clazz : classes)
            register(clazz);
    }

    public void initUsers() throws Exception {
        @NotNull final String adminHash = userService.getHashOfPassword(ADMIN);
        @NotNull final String userHash = userService.getHashOfPassword(USER);
        userService.persist(new User(ADMIN, adminHash, Role.ROLE_ADMIN));
        userService.persist(new User(USER, userHash, Role.ROLE_USER));
    }

    public void start() throws Exception {
        System.out.println(GREETING);
        @NotNull String command = "";
        while (!EXIT.equals(command)) {
            command = ConsoleUtil.getStringFromConsole();
            try {
                execute(command);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;

        final boolean commandIsNotSafe = !abstractCommand.isSafe();
        if (commandIsNotSafe && currentUser == null)
            throw new UserNotAuthorized();

        final boolean userAuthorizedAndHasNotAccessRight = currentUser != null &&
                !abstractCommand.getRoles().contains(currentUser.getRole());
        if (commandIsNotSafe && userAuthorizedAndHasNotAccessRight) throw new UserHasNotAccess();
        abstractCommand.execute();
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public ProjectService getProjectService() {
        return projectService;
    }

    @NotNull
    @Override
    public TaskService getTaskService() {
        return taskService;
    }

    @NotNull
    @Override
    public UserService getUserService() {
        return userService;
    }

    @Nullable
    public User getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(@Nullable final User currentUser) {
        this.currentUser = currentUser;
    }
}
