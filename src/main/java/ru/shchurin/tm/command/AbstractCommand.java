package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.ServiceLocator;
import ru.shchurin.tm.entity.Role;

import java.util.List;

public abstract class AbstractCommand {
    @NotNull
    static final String ENTER_NAME = "ENTER NAME:";

    @NotNull
    static final String ENTER_DESCRIPTION = "ENTER DESCRIPTION:";

    @NotNull
    static final String ENTER_START_DATE = "ENTER START_DATE:";

    @NotNull
    static final String ENTER_END_DATE = "ENTER END_DATE:";

    @NotNull
    static final String ENTER_ID = "ENTER ID:";

    @NotNull
    static final String ENTER_PROJECT_ID = "ENTER PROJECT_ID:";

    @NotNull
    static final String ENTER_CREATION_DATE = "ENTER CREATION_DATE:";

    @NotNull
    static final String ENTER_PROJECT_NAME = "ENTER PROJECT NAME:";

    @NotNull
    static final String ENTER_LOGIN = "ENTER LOGIN:";

    @NotNull
    static final String ENTER_PASSWORD = "ENTER PASSWORD:";

    @NotNull
    static final String WRONG_START_DATE = "YOU ENTERED WRONG START_DATE:";

    @NotNull
    static final String WRONG_END_DATE = "YOU ENTERED WRONG END_DATE:";

    @NotNull
    static final String WRONG_CREATION_DATE = "YOU ENTERED WRONG CREATION_DATE:";

    @NotNull
    static final String FIND_PROJECT = "[FIND PROJECT]";

    @NotNull
    static final String FIND_TASK = "[FIND TASK]";

    @Nullable
    protected ServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getCommand();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws Exception;

    public abstract boolean isSafe();

    @NotNull
    public abstract List<Role> getRoles();
}
