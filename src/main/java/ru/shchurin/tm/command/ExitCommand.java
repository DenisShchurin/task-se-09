package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;
import java.util.ArrayList;
import java.util.List;

public final class ExitCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "exit";

    @NotNull
    private static final String DESCRIPTION = "Exit the application.";

    @NotNull
    private static final String EXIT = "[GOOD BYE]";

    private final boolean safe = true;

    @NotNull
    private final List<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println(EXIT);
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
