package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.util.DateUtil;
import ru.shchurin.tm.exception.*;

import java.text.ParseException;
import java.util.*;

public final class ProjectUpdateCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "project-update";

    @NotNull
    private static final String DESCRIPTION = "Update project.";

    @NotNull
    private static final String PROJECT_UPDATE = "[PROJECT UPDATE]";

    @NotNull
    private static final String PROJECT_UPDATED = "[PROJECT UPDATED]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(PROJECT_UPDATE);
        System.out.println(ENTER_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_DESCRIPTION);
        @Nullable final String description = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_ID);
        @Nullable final String id = ConsoleUtil.getStringFromConsole();
        System.out.println(ENTER_START_DATE);
        @Nullable final String start = ConsoleUtil.getStringFromConsole();
        @Nullable final Date startDate;
        try {
            startDate = DateUtil.parseDate(start);
        } catch (ParseException e) {
            System.out.println(WRONG_START_DATE);
            return;
        }
        System.out.println(ENTER_END_DATE);
        @Nullable final String end = ConsoleUtil.getStringFromConsole();
        @Nullable final Date endDate;
        try {
            endDate = DateUtil.parseDate(end);
        } catch (ParseException e) {
            System.out.println(WRONG_END_DATE);
            return;
        }
        System.out.println(ENTER_CREATION_DATE);
        @Nullable final String creation = ConsoleUtil.getStringFromConsole();
        @Nullable final Date creationDate;
        try {
            creationDate = DateUtil.parseDate(creation);
        } catch (ParseException e) {
            System.out.println(WRONG_CREATION_DATE);
            return;
        }

        @Nullable final Project project = new Project(id, name, description, startDate, endDate,
                ((Bootstrap)serviceLocator).getCurrentUser().getId(), creationDate);
        try {
            serviceLocator.getProjectService().merge(project);
            System.out.println(PROJECT_UPDATED);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
