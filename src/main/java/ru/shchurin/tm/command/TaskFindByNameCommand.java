package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.util.ConsoleUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskFindByNameCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "task-find-by-name";

    @NotNull
    private static final String DESCRIPTION = "Find task by part of the name.";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(FIND_TASK);
        System.out.println(ENTER_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        @NotNull final String currentUserId = ((Bootstrap) serviceLocator).getCurrentUser().getId();

        @NotNull final List<Task> tasks = serviceLocator.getTaskService().findByName(name, currentUserId);
        for(@NotNull final Task task : tasks) {
            System.out.println(task);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
