package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskListCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "task-list";

    @NotNull
    private static final String DESCRIPTION = "Show all tasks.";

    @NotNull
    private static final String TASK_LIST = "[TASK LIST]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        @NotNull final User currentUser = ((Bootstrap)serviceLocator).getCurrentUser();
        System.out.println(TASK_LIST);
        int index = 1;
        for (@NotNull final Task task : serviceLocator.getTaskService().findAll(currentUser.getId())) {
            if (task.getUserId().equals(currentUser.getId())) {
                System.out.println(index++ + ". " + task);
            }
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
