package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.Task;
import ru.shchurin.tm.util.ConsoleUtil;
import ru.shchurin.tm.exception.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TasksOfProjectCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "tasks-of-project";

    @NotNull
    private static final String DESCRIPTION = "Show all tasks of project.";

    @NotNull
    private static final String TASKS_OF_PROJECT = "[TASKS OF PROJECT]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_USER));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws Exception {
        System.out.println(TASKS_OF_PROJECT);
        System.out.println(ENTER_PROJECT_NAME);
        @Nullable final String name = ConsoleUtil.getStringFromConsole();
        @NotNull final List<Task> tasks;
        try {
            tasks = serviceLocator.getProjectService().getTasksOfProject(((Bootstrap)serviceLocator).getCurrentUser().getId(),
                    name);
            int index = 1;
            for (@NotNull final Task task : tasks) {
                System.out.println(index++ + ". " + task);
            }
        } catch (ConsoleNameException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
