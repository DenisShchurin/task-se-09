package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class UserListCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "user-list";

    @NotNull
    private static final String DESCRIPTION = "Show all users.";

    @NotNull
    private static final String USER_LIST = "[USER LIST]";

    private final boolean safe = false;

    @NotNull
    private final List<Role> roles = new ArrayList<>(Collections.singletonList(Role.ROLE_ADMIN));

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println(USER_LIST);
        int index = 1;
        for (@NotNull final User user: serviceLocator.getUserService().findAll()) {
            System.out.println(index++ + ". " + user);
        }
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
