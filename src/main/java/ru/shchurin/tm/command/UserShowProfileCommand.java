package ru.shchurin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.bootstrap.Bootstrap;
import ru.shchurin.tm.entity.Role;
import ru.shchurin.tm.entity.User;

import java.util.ArrayList;
import java.util.List;

public final class UserShowProfileCommand extends AbstractCommand {
    @NotNull
    private static final String COMMAND = "user-show-profile";

    @NotNull
    private static final String DESCRIPTION = "Show user profile.";

    @NotNull
    private static final String SHOW_USER_PROFILE = "[SHOW USER PROFILE]";

    private final boolean safe = false;

    @NotNull
    private final ArrayList<Role> roles = new ArrayList<>();

    @NotNull
    @Override
    public String getCommand() {
        return COMMAND;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @NotNull final User currentUser = ((Bootstrap)serviceLocator).getCurrentUser();
        System.out.println(SHOW_USER_PROFILE);
        System.out.println(currentUser.getLogin() + ", " + currentUser.getRole());
    }

    @Override
    public boolean isSafe() {
        return safe;
    }

    @NotNull
    @Override
    public List<Role> getRoles() {
        return roles;
    }
}
