package ru.shchurin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Project;

import java.util.Comparator;

public final class ProjectStatusComparator implements Comparator<Project> {
    @Override
    public int compare(@NotNull final Project o1, @NotNull final Project o2) {
        return o1.getStatus().compareTo(o2.getStatus());
    }
}
