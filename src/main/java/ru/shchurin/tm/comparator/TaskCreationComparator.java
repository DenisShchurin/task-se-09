package ru.shchurin.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.shchurin.tm.entity.Task;

import java.util.Comparator;

public final class TaskCreationComparator implements Comparator<Task> {
    @Override
    public int compare(@NotNull final Task o1, @NotNull final Task o2) {
        return o1.getCreationDate().compareTo(o2.getCreationDate());
    }
}

