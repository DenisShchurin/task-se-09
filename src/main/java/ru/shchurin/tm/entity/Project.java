package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.util.DateUtil;

import java.util.Date;
@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date startDate;

    @Nullable
    private Date endDate;

    @Nullable
    private String userId;

    @Nullable
    private Status status = Status.STATUS_SCHEDULED;

    @Nullable
    private Date creationDate = new Date();

    public Project(
        @Nullable final String id, @Nullable final String name, @Nullable final String description,
        @Nullable final Date startDate, @Nullable final Date endDate, @Nullable final String userId,
        @Nullable final Date creationDate
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
        this.creationDate = creationDate;
    }

    public Project(
        @Nullable final String name, @Nullable final String description, @Nullable final Date startDate,
        @Nullable final Date endDate, @Nullable final String userId
    ) {
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return "Project{" +
                ", ID='" + id + '\'' +
                "project name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + DateUtil.dateFormat(startDate) +
                ", endDate=" + DateUtil.dateFormat(endDate) +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", creationDate=" + DateUtil.dateFormat(creationDate) +
                '}';
    }
}
