package ru.shchurin.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.util.DateUtil;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private Date startDate;

    @Nullable
    private Date endDate;

    @Nullable
    private String projectId;

    @Nullable
    private String userId;

    @Nullable
    private Status status = Status.STATUS_SCHEDULED;

    @Nullable
    private Date creationDate = new Date();

    public Task(
        @Nullable String id, @Nullable final String name, @Nullable final String description,
        @Nullable final String projectId, @Nullable final Date startDate, @Nullable final Date endDate,
        @Nullable final String userId, @Nullable final Date creationDate
    ) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startDate = startDate;
        this.endDate = endDate;
        this.projectId = projectId;
        this.userId = userId;
        this.creationDate = creationDate;
    }

    public Task(
        @Nullable final String name, @Nullable final String description, @Nullable final String projectId,
        @Nullable final Date startDate, @Nullable final Date endDate, @Nullable final String userId
    ) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
    }

    @NotNull
    @Override
    public String toString() {
        return "Task{" +
                ", ID='" + id + '\'' +
                "task name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", projectId='" + projectId + '\'' +
                ", userId='" + userId + '\'' +
                ", status=" + status +
                ", creationDate=" + creationDate +
                '}';
    }
}
