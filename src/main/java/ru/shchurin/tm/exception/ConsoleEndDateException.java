package ru.shchurin.tm.exception;

public final class ConsoleEndDateException extends Exception {
    public ConsoleEndDateException() {
        super("YOU ENTERED INCORRECT START_DATE");
    }
}
