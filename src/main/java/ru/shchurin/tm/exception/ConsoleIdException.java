package ru.shchurin.tm.exception;

public final class ConsoleIdException extends Exception {
    public ConsoleIdException() {
        super("YOU ENTERED INCORRECT ID");
    }
}
