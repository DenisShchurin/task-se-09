package ru.shchurin.tm.exception;

public final class ConsoleStartDateException extends Exception {
    public ConsoleStartDateException() {
        super("YOU ENTERED INCORRECT START_DATE");
    }
}
