package ru.shchurin.tm.exception;

public final class TaskNotFoundException extends Exception {
    public TaskNotFoundException() {
        super("Task not found");
    }
}
