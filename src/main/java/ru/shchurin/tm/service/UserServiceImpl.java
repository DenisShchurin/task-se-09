package ru.shchurin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.shchurin.tm.api.UserService;
import ru.shchurin.tm.entity.User;
import ru.shchurin.tm.exception.*;
import ru.shchurin.tm.api.UserRepository;
import ru.shchurin.tm.util.HashUtil;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;

public final class UserServiceImpl implements UserService {
    @NotNull
    private final UserRepository userRepository;

    public UserServiceImpl(@NotNull final UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public Collection<User> findAll() {
        return userRepository.findAll();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        return userRepository.findOne(id);
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if (user == null) {
            return;
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new ConsoleLoginException();
        }
        if (user.getHashPassword() == null || user.getHashPassword().isEmpty()) {
            throw new ConsoleHashPasswordException();
        }
        userRepository.persist(user);
    }

    @Override
    public void merge(@Nullable final User user) throws ConsoleLoginException {
        if (user == null) {
            return;
        }
        if (user.getLogin() == null || user.getLogin().isEmpty()) {
            throw new ConsoleLoginException();
        }
        userRepository.merge(user);
    }

    @Override
    public void remove(@Nullable final String id) throws ConsoleIdException {
        if (id == null || id.isEmpty()) {
            throw new ConsoleIdException();
        }
        userRepository.remove(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @Override
    public void removeByLogin(@Nullable final String login) throws ConsoleLoginException {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        userRepository.removeByLogin(login);
    }

    @NotNull
    @Override
    public String getHashOfPassword(@Nullable final String password)
            throws NoSuchAlgorithmException, ConsolePasswordException {
        if (password == null || password.isEmpty()) {
            throw new ConsolePasswordException();
        }
        return HashUtil.getHash(password);
    }

    @Nullable
    @Override
    public User authoriseUser(@Nullable final String login, @Nullable final String hashOfPassword)
            throws ConsoleLoginException, ConsoleHashPasswordException {
        if (login == null || login.isEmpty()) {
            throw new ConsoleLoginException();
        }
        if (hashOfPassword == null || hashOfPassword.isEmpty()) {
            throw new ConsoleHashPasswordException();
        }
        @NotNull final Collection<User> users = userRepository.findAll();
        for (@NotNull final User user : users) {
            if (login.equals(user.getLogin()) && hashOfPassword.equals(user.getHashPassword())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public boolean updatePassword(
            @Nullable final String login, @Nullable final String hashPassword, @Nullable final String newHashPassword
    )       throws UserNotAuthorized, ConsolePasswordException {
        if (login == null || login.isEmpty())
            throw new UserNotAuthorized();
        if (hashPassword == null || hashPassword.isEmpty())
            throw new UserNotAuthorized();
        if (newHashPassword == null || newHashPassword.isEmpty())
            throw new ConsolePasswordException();
        return userRepository.updatePassword(login, hashPassword, newHashPassword);
    }
}
