package ru.shchurin.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public final class ConsoleUtil {
    @NotNull
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    @Nullable
    public static String getStringFromConsole() throws IOException {
        return reader.readLine();
    }
}
